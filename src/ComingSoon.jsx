import './App.css'
const ComingSoon = () => {
  return (
    <h1>
      <span>C</span>
      <span>O</span>
      <span>M</span>
      <span>I</span>
      <span>N</span>
      <span>G</span>
      <span style={{ marginLeft: '2rem' }}>S</span>
      <span>O</span>
      <span>O</span>
      <span>N</span>
    </h1>
  )
}

export default ComingSoon
