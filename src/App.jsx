import { useState } from 'react'
import ComingSoon from './ComingSoon'
import logo from './logo.svg'

function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">
      <ComingSoon />
    </div>
  )
}

export default App
